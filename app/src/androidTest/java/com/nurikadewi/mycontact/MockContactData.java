package com.nurikadewi.mycontact;

import com.google.gson.Gson;
import com.nurikadewi.mycontact.mvp.model.Contact;

/**
 * Created by nurikadewi on 8/3/17.
 */

public class MockContactData {

    public static final String CONTACT_LIST_DATA_JSON = "[\n" +
            "    {\n" +
            "        \"id\": 618,\n" +
            "        \"first_name\": \"Contact\",\n" +
            "        \"last_name\": \"Favourite\",\n" +
            "        \"profile_pic\": \"/images/missing.png\",\n" +
            "        \"favorite\": true,\n" +
            "        \"url\": \"http://gojek-contacts-app.herokuapp.com/contacts/618.json\"\n" +
            "    },\n" +
            "    {\n" +
            "        \"id\": 393,\n" +
            "        \"first_name\": \"Giiiooo\",\n" +
            "        \"last_name\": \"Jjjjjl\",\n" +
            "        \"profile_pic\": \"https://contacts-app.s3-ap-southeast-1.amazonaws.com/contacts/profile_pics/000/000/393/original/IMG_20170720_014702.jpg?1500842746\",\n" +
            "        \"favorite\": true,\n" +
            "        \"url\": \"http://gojek-contacts-app.herokuapp.com/contacts/393.json\"\n" +
            "    }\n" +
            "]";

    public static final String CONTACT_LIST_DATA_JSON_EMPTY = "[]";
}
