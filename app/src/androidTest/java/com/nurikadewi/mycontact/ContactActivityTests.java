package com.nurikadewi.mycontact;

import android.content.Context;
import android.content.res.Resources;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.DataInteraction;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.ViewAssertion;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import com.nurikadewi.mycontact.modules.contact.ContactActivity;
import com.nurikadewi.mycontact.modules.contact.adapter.ContactAdapter;
import com.nurikadewi.mycontact.mvp.model.Contact;
import com.nurikadewi.mycontact.mvp.presenter.ContactPresenter;
import com.nurikadewi.mycontact.utilities.NetworkUtils;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isClickable;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.AllOf.allOf;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsAnything.anything;
import static org.hamcrest.core.IsNull.notNullValue;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/2/17.
 */
@RunWith(AndroidJUnit4.class)
public class ContactActivityTests {

    private Context mContext;

    /**
     * Launches {@link ContactActivity} for every test
     */
    @Rule
    public ActivityTestRule<ContactActivity> activityRule = new ActivityTestRule<>(ContactActivity.class);

    /*
     * Sets up global values before test runs. Will be called before EVERY test.
     */
    @Before
    public void runBeforeTest() throws Exception {
        mContext = InstrumentationRegistry.getTargetContext();
    }

    @Test
    public void checkLayout() {
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()));
        onView(withId(R.id.fab_add_contact)).check(matches(isDisplayed()));
        onView(withId(R.id.rv_contact_list))
                .check(matches(notNullValue()))
                .check(matches(isDisplayed()));
        onView(withId(R.id.text_no_contacts)).check(matches(not(isDisplayed())));
    }

    /**
     * Checks whether a button is enabled.
     */
    @Test
    public void testIsDisplayedViewOnTop() {

       /* if (NetworkUtils.isNetAvailable(mContext)) {
        } else {

        }
        DataInteraction dataInteraction = onData(is(instanceOf(Contact.class)))
                .inAdapterView(allOf(withId(R.id.rv_contact_list), isDisplayed()));*/

               /* .atPosition(0)
                .check(matches(isDisplayed()));*/


        //onView(withId(R.id.rv_contact_list)).perform(RecyclerViewActions.
        /*if (mContactList.getAdapter().getItemCount() > 0) {
            onView(withId(R.id.rv_contact_list)).check(matches(isDisplayed()));
            onView(withId(R.id.text_no_contacts)).check(matches(not(isDisplayed())));
        } else {
            onView(withId(R.id.text_no_contacts)).check(matches(isDisplayed()));
            onView(withId(R.id.rv_contact_list)).check(matches(not(isDisplayed())));
        }*/

    }

    /**
     * Test a Widget is clickable.
     */
    @Test
    public void testIsClickable() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        if (NetworkUtils.isNetAvailable(appContext)) {
            onView(withId(R.id.fab_add_contact)).check(matches(isClickable()));
        }
    }

    /**
     * Test a Add Contact is Click
     */
    @Test
    public void testAddContactClicked() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        if (NetworkUtils.isNetAvailable(appContext)) {
            onView(withId(R.id.fab_add_contact)).perform(click());
        }
    }

    /**
     * Checks Contact Click Test
     */
    @Test
    public void contactClickTest() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        if (NetworkUtils.isNetAvailable(appContext)) {
            onView(withId(R.id.rv_contact_list)).perform(RecyclerViewActions.actionOnItemAtPosition(2, click()));
        }
    }
}

