package com.nurikadewi.mycontact.api;

import com.nurikadewi.mycontact.mvp.model.Contact;
import com.nurikadewi.mycontact.mvp.model.ContactDetail;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/1/17.
 */

public interface IApiService {

    @GET("/contacts.json")
    Observable<Contact[]> getContacts();

    @GET("/contacts/{id}.json")
    Observable<ContactDetail> getContact(@Path("id") String id);
}
