package com.nurikadewi.mycontact.mvp.model;

import java.io.Serializable;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/1/17.
 */

public class ContactDetail implements Serializable {


    /**
     * id : 322
     * first_name : Abc1Qqwyyyyttt
     * last_name : Def1Kkyyt
     * email :
     * phone_number :
     * profile_pic : https://contacts-app.s3-ap-southeast-1.amazonaws.com/contacts/profile_pics/000/000/322/original/profile.png?1499623287
     * favorite : true
     * created_at : 2017-07-03T15:25:33.677Z
     * updated_at : 2017-07-30T19:09:23.343Z
     */

    private int id;
    private String first_name;
    private String last_name;
    private String email;
    private String phone_number;
    private String profile_pic;
    private boolean favorite;
    private String created_at;
    private String updated_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
