package com.nurikadewi.mycontact.mvp.view;

import android.content.Intent;

import com.nurikadewi.mycontact.mvp.model.Contact;

import java.util.List;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/1/17.
 */

public interface IContactView extends IBaseView {

    void onContactsLoaded(List<Contact> contacts);

    void onShowDialog(String message);

    void onShowAlertDialog(String title, String message);

    void onHideDialog();

    void onShowToast(String message);

    void onClearItems();

    void onCallIntent(Intent intent);
}