package com.nurikadewi.mycontact.mvp.view;

import com.nurikadewi.mycontact.mvp.model.ContactDetail;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/2/17.
 */

public interface IDetailContactView extends IBaseView {

    void onContactLoaded(ContactDetail contact);

    void onShowDialog(String message);

    void onShowAlertDialog(String title, String message);

    void onHideDialog();

    void onShowToast(String message);
}
