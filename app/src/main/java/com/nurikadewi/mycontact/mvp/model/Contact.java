package com.nurikadewi.mycontact.mvp.model;

import java.io.Serializable;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/1/17.
 */

public class Contact implements Serializable {
    /**
     * id : 322
     * first_name : Abc1Qqwyyyyttt
     * last_name : Def1Kkyyt
     * profile_pic : https://contacts-app.s3-ap-southeast-1.amazonaws.com/contacts/profile_pics/000/000/322/original/profile.png?1499623287
     * favorite : true
     * url : http://gojek-contacts-app.herokuapp.com/contacts/322.json
     */

    private int id;
    private String first_name;
    private String last_name;
    private String profile_pic;
    private boolean favorite;
    private String url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
