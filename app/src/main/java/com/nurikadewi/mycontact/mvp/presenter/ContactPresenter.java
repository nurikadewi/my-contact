package com.nurikadewi.mycontact.mvp.presenter;

import com.nurikadewi.mycontact.api.IApiService;
import com.nurikadewi.mycontact.base.BasePresenter;
import com.nurikadewi.mycontact.mapper.ContactMapper;
import com.nurikadewi.mycontact.mvp.model.Contact;
import com.nurikadewi.mycontact.mvp.model.Storage;
import com.nurikadewi.mycontact.mvp.view.IContactView;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/1/17.
 */

public class ContactPresenter extends BasePresenter<IContactView> implements Observer<Contact[]> {

    @Inject
    protected IApiService mApiService;
    @Inject
    protected ContactMapper mContactMapper;
    @Inject
    protected Storage mStorage;

    @Inject
    protected ContactPresenter() {
    }

    public void getContacts() {
        getView().onShowDialog("Please Wait..");
        Observable<Contact[]> ContactsResponseObservable = mApiService.getContacts();
        subscribe(ContactsResponseObservable, this);
    }

    @Override
    public void onCompleted() {
        getView().onHideDialog();
    }

    @Override
    public void onError(Throwable e) {
        getView().onHideDialog();
        getView().onShowToast("Error loading data : " + e.getMessage());
    }

    @Override
    public void onNext(Contact[] contactList) {
        List<Contact> contacts = mContactMapper.mapContacts(mStorage, contactList);
        getView().onClearItems();
        getView().onContactsLoaded(contacts);
    }

    public void onErrorNetwork() {
        getView().onShowAlertDialog("Network Error", "Unable to contact the server");
    }

    public void getContactsFromDatabase() {
        List<Contact> contacts = mStorage.getSavedContacts();
        getView().onClearItems();
        getView().onContactsLoaded(contacts);
    }
}
