package com.nurikadewi.mycontact.mvp.model;

/**
 * Created by nurikadewi on 8/4/17.
 */

public class ContactResponse {

    private Contact[] contacts;

    public Contact[] getContacts() {
        return contacts;
    }

    public void setContacts(Contact[] contacts) {
        this.contacts = contacts;
    }
}
