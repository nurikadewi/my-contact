package com.nurikadewi.mycontact.mvp.presenter;

import com.nurikadewi.mycontact.api.IApiService;
import com.nurikadewi.mycontact.base.BasePresenter;
import com.nurikadewi.mycontact.mapper.ContactMapper;
import com.nurikadewi.mycontact.mvp.model.Contact;
import com.nurikadewi.mycontact.mvp.model.ContactDetail;
import com.nurikadewi.mycontact.mvp.model.Storage;
import com.nurikadewi.mycontact.mvp.view.IContactView;
import com.nurikadewi.mycontact.mvp.view.IDetailContactView;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/2/17.
 */

public class DetailPresenter extends BasePresenter<IDetailContactView> implements Observer<ContactDetail> {

    @Inject
    protected IApiService mApiService;

    @Inject
    protected DetailPresenter() {
    }

    public void getContact(String id) {
        getView().onShowDialog("Please Wait..");
        Observable<ContactDetail> ContactsResponseObservable = mApiService.getContact(id);
        subscribe(ContactsResponseObservable, this);
    }

    @Override
    public void onCompleted() {
        getView().onHideDialog();
    }

    @Override
    public void onError(Throwable e) {
        getView().onHideDialog();
        getView().onShowToast("Error loading data : " + e.getMessage());
    }

    @Override
    public void onNext(ContactDetail contact) {
        getView().onContactLoaded(contact);
    }

    public void onErrorNetwork() {
        getView().onShowAlertDialog("Network Error", "Unable to contact the server");
    }
}
