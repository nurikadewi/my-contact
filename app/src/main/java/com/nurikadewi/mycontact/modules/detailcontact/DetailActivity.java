package com.nurikadewi.mycontact.modules.detailcontact;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nurikadewi.mycontact.R;
import com.nurikadewi.mycontact.application.ContactApp;
import com.nurikadewi.mycontact.base.BaseActivity;
import com.nurikadewi.mycontact.di.components.DaggerIDetailComponent;
import com.nurikadewi.mycontact.di.module.ContactModule;
import com.nurikadewi.mycontact.di.module.DetailModule;
import com.nurikadewi.mycontact.mvp.model.Contact;
import com.nurikadewi.mycontact.mvp.model.ContactDetail;
import com.nurikadewi.mycontact.mvp.presenter.ContactPresenter;
import com.nurikadewi.mycontact.mvp.presenter.DetailPresenter;
import com.nurikadewi.mycontact.mvp.view.IDetailContactView;
import com.nurikadewi.mycontact.utilities.NetworkUtils;

import javax.inject.Inject;

import butterknife.Bind;

public class DetailActivity extends BaseActivity implements IDetailContactView {

    @Bind(R.id.toolbar)
    protected Toolbar mToolbar;
    @Bind(R.id.text_phone_no)
    protected TextView mTextPhoneNo;
    @Bind(R.id.text_email_address)
    protected TextView mTextEmailAddress;
    @Bind(R.id.icon_call)
    protected ImageView mIcoCall;
    @Bind(R.id.icon_mail)
    protected ImageView mIcoMail;
    @Bind(R.id.icon_message)
    protected ImageView mIconMessage;


    @Inject
    protected DetailPresenter mPresenter;

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        initializeContactInfo();
        loadContact();
    }

    private void loadContact() {
        String id = getIntent().getStringExtra(ContactApp.CONTACT_ID);
        if (id != null) {
            if (NetworkUtils.isNetAvailable(this)) {
                mPresenter.getContact(id);
            } else {
                mPresenter.onErrorNetwork();
            }
        }
    }

    private void initializeContactInfo() {
    }

    @Override
    public int getContentView() {
        return R.layout.activity_detail;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void resolveDaggerDependency() {
        DaggerIDetailComponent.builder()
                .iAppComponent(getApplicationComponent())
                .detailModule(new DetailModule(this))
                .build().inject(this);
    }

    @Override
    public void onContactLoaded(ContactDetail contact) {
        showToolbar(mToolbar, 0, contact.getFirst_name() + " " + contact.getLast_name());
        mTextPhoneNo.setText(contact.getPhone_number());
        mTextEmailAddress.setText(contact.getEmail());
    }

    @Override
    public void onShowDialog(String message) {
        showDialog(message);
    }

    @Override
    public void onShowAlertDialog(String title, String message) {
        showAlertDialog(title, message);
    }

    @Override
    public void onHideDialog() {
        hideDialog();
    }

    @Override
    public void onShowToast(String message) {
        Toast.makeText(DetailActivity.this, message, Toast.LENGTH_SHORT).show();
    }

}
