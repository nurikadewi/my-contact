package com.nurikadewi.mycontact.modules.addcontact;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.nurikadewi.mycontact.R;

public class AddContactActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
    }
}
