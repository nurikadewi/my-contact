package com.nurikadewi.mycontact.modules.contact;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.nurikadewi.mycontact.R;
import com.nurikadewi.mycontact.application.ContactApp;
import com.nurikadewi.mycontact.base.BaseActivity;
import com.nurikadewi.mycontact.di.components.DaggerIContactComponent;
import com.nurikadewi.mycontact.di.module.ContactModule;
import com.nurikadewi.mycontact.modules.addcontact.AddContactActivity;
import com.nurikadewi.mycontact.modules.contact.adapter.ContactAdapter;
import com.nurikadewi.mycontact.modules.detailcontact.DetailActivity;
import com.nurikadewi.mycontact.mvp.model.Contact;
import com.nurikadewi.mycontact.mvp.presenter.ContactPresenter;
import com.nurikadewi.mycontact.mvp.view.IContactView;
import com.nurikadewi.mycontact.utilities.NetworkUtils;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;

public class ContactActivity extends BaseActivity implements IContactView, View.OnClickListener {

    @Bind(R.id.toolbar)
    protected Toolbar mToolbar;
    @Bind(R.id.rv_contact_list)
    protected RecyclerView mContactList;
    @Bind(R.id.fab_add_contact)
    protected FloatingActionButton mAddContact;
    @Bind(R.id.text_no_contacts)
    protected TextView mNoContacts;
    @Inject
    protected ContactPresenter mPresenter;
    private ContactAdapter mContactAdapter;

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        showToolbar(mToolbar, R.drawable.ic_menu, getString(R.string.app_name));
        mAddContact.setOnClickListener(this);
        initializeList();
        loadContacts();
    }

    private void loadContacts() {
        if (NetworkUtils.isNetAvailable(this)) {
            mPresenter.getContacts();
        } else {
            //mPresenter.onErrorNetwork();
            mPresenter.getContactsFromDatabase();
        }
    }

    private void initializeList() {
        mContactList.setHasFixedSize(true);
        mContactList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mContactAdapter = new ContactAdapter(getLayoutInflater());
        mContactAdapter.setContactClickListener(mContactClickListener);
        mContactList.setAdapter(mContactAdapter);
    }

    @Override
    public int getContentView() {
        return R.layout.activity_contact;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_contact, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void resolveDaggerDependency() {
        DaggerIContactComponent.builder()
                .iAppComponent(getApplicationComponent())
                .contactModule(new ContactModule(this))
                .build().inject(this);
    }

    @Override
    public void onContactsLoaded(List<Contact> contacts) {
        if (contacts.size() == 0) {
            mNoContacts.setVisibility(View.VISIBLE);
            mContactList.setVisibility(View.GONE);
        } else {
            mContactList.setVisibility(View.VISIBLE);
            mNoContacts.setVisibility(View.GONE);
            mContactAdapter.addContacts(contacts);
        }
    }

    @Override
    public void onShowDialog(String message) {
        showDialog(message);
    }

    @Override
    public void onShowAlertDialog(String title, String message) {
        showAlertDialog(title, message);
    }

    @Override
    public void onHideDialog() {
        hideDialog();
    }

    @Override
    public void onShowToast(String message) {
        Toast.makeText(ContactActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClearItems() {
        mContactAdapter.clearContact();
    }

    @Override
    public void onCallIntent(Intent intent) {
        showIntent(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab_add_contact:
                onCallIntent(new Intent(ContactActivity.this, AddContactActivity.class));
                break;
        }
    }

    private ContactAdapter.OnContactClickListener mContactClickListener = new ContactAdapter.OnContactClickListener() {
        @Override
        public void onClick(View v, Contact contact, int position) {
            Intent i = new Intent(ContactActivity.this, DetailActivity.class);
            i.putExtra(ContactApp.CONTACT_ID, String.valueOf(contact.getId()));
            onCallIntent(i);
        }
    };

}
