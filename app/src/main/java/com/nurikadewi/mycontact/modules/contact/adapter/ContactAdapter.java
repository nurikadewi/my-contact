package com.nurikadewi.mycontact.modules.contact.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nurikadewi.mycontact.R;
import com.nurikadewi.mycontact.helper.ImageHandler;
import com.nurikadewi.mycontact.mvp.model.Contact;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/1/17.
 */

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.Holder> {

    private LayoutInflater mLayoutInflater;
    private List<Contact> mContactList = new ArrayList<>();

    public ContactAdapter(LayoutInflater inflater) {
        mLayoutInflater = inflater;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.list_item_layout, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.bind(mContactList.get(position));
    }

    @Override
    public int getItemCount() {
        return mContactList.size();
    }

    public void addContacts(List<Contact> contacts) {
        mContactList.addAll(contacts);
        notifyDataSetChanged();
    }

    public void clearContact() {
        mContactList.clear();
        notifyDataSetChanged();
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.image_contact_favorite)
        protected ImageView mImageContactFavorite;
        @Bind(R.id.text_initial)
        protected TextView mTextInitial;
        @Bind(R.id.text_contact_fname)
        protected TextView mTextContactFname;
        @Bind(R.id.text_contact_lname)
        protected TextView mTextContactLname;

        private Context mContext;
        private Contact mContact;

        public Holder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mContext = itemView.getContext();
            ButterKnife.bind(this, itemView);
        }

        public void bind(Contact contact) {
            mContact = contact;
            mTextContactFname.setText(contact.getFirst_name());
            mTextContactLname.setText(contact.getLast_name());
            mTextInitial.setText(String.valueOf(contact.getFirst_name().charAt(0)));

            if (!contact.isFavorite()) {
                mImageContactFavorite.setVisibility(View.INVISIBLE);
            } else {
                mImageContactFavorite.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onClick(View v) {
            if (mContactClickListener != null) {
                mContactClickListener.onClick(mTextInitial, mContact, getAdapterPosition());
            }
        }
    }

    public void setContactClickListener(OnContactClickListener listener) {
        mContactClickListener = listener;
    }

    private OnContactClickListener mContactClickListener;

    public interface OnContactClickListener {

        void onClick(View v, Contact contact, int position);
    }
}
