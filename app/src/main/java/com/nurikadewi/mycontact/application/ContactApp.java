package com.nurikadewi.mycontact.application;

import android.app.Application;

import com.nurikadewi.mycontact.di.components.DaggerIAppComponent;
import com.nurikadewi.mycontact.di.components.IAppComponent;
import com.nurikadewi.mycontact.di.module.AppModule;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/1/17.
 */

public class ContactApp extends Application {

    private IAppComponent mIAppComponent;
    public static final String CONTACT_ID = "contact_id";

    @Override
    public void onCreate() {
        super.onCreate();
        initializeApplicationComponent();
    }

    private void initializeApplicationComponent() {
        mIAppComponent = DaggerIAppComponent
                .builder()
                .appModule(new AppModule(this, "http://gojek-contacts-app.herokuapp.com"))
                .build();
    }

    public IAppComponent getApplicationComponent() {
        return mIAppComponent;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
