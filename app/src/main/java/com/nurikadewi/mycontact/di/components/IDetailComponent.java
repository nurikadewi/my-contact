package com.nurikadewi.mycontact.di.components;

import com.nurikadewi.mycontact.di.module.DetailModule;
import com.nurikadewi.mycontact.di.scope.PerActivity;
import com.nurikadewi.mycontact.modules.detailcontact.DetailActivity;

import dagger.Component;


/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/2/17.
 */

@PerActivity
@Component(modules = DetailModule.class, dependencies = IAppComponent.class)
public interface IDetailComponent {

    void inject(DetailActivity activity);
}
