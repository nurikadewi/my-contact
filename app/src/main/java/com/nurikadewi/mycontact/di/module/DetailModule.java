package com.nurikadewi.mycontact.di.module;

import com.nurikadewi.mycontact.api.IApiService;
import com.nurikadewi.mycontact.di.scope.PerActivity;
import com.nurikadewi.mycontact.mvp.view.IDetailContactView;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/2/17.
 */

@Module
public class DetailModule {

    private IDetailContactView mView;

    public DetailModule(IDetailContactView view) {
        mView = view;
    }

    @PerActivity
    @Provides
    IApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(IApiService.class);
    }

    @PerActivity
    @Provides
    IDetailContactView provideView() {
        return mView;
    }
}
