package com.nurikadewi.mycontact.di.components;

import com.nurikadewi.mycontact.di.module.ContactModule;
import com.nurikadewi.mycontact.di.scope.PerActivity;
import com.nurikadewi.mycontact.modules.contact.ContactActivity;

import dagger.Component;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/1/17.
 */

@PerActivity
@Component(modules = ContactModule.class, dependencies = IAppComponent.class)
public interface IContactComponent {

    void inject(ContactActivity activity);
}
