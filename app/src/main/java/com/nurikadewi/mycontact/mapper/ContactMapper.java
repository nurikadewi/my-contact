package com.nurikadewi.mycontact.mapper;

import com.nurikadewi.mycontact.mvp.model.Contact;
import com.nurikadewi.mycontact.mvp.model.Storage;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/1/17.
 */

public class ContactMapper {

    @Inject
    public ContactMapper() {
    }

    public List<Contact> mapContacts(Storage storage, Contact[] response) {
        List<Contact> contactList = new ArrayList<>();

        if (response != null) {
            for (Contact contacts : response) {
                Contact contact = new Contact();
                contact.setId(contacts.getId());
                contact.setFirst_name(contacts.getFirst_name());
                contact.setLast_name(contacts.getLast_name());
                contact.setProfile_pic(contacts.getProfile_pic());
                contact.setFavorite(contacts.isFavorite());
                contact.setUrl(contacts.getUrl());
                storage.addContact(contact);
                contactList.add(contact);
            }
        }
        return contactList;
    }
}