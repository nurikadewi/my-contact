package com.nurikadewi.mycontact.mvp.presenter;

import android.os.Looper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import rx.Observable;
import rx.Scheduler;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.plugins.RxJavaSchedulersHook;
import rx.schedulers.Schedulers;


import com.nurikadewi.mycontact.api.IApiService;
import com.nurikadewi.mycontact.mapper.ContactMapper;
import com.nurikadewi.mycontact.mvp.model.Contact;
import com.nurikadewi.mycontact.mvp.model.ContactResponse;
import com.nurikadewi.mycontact.mvp.view.IContactView;
import com.nurikadewi.mycontact.mvp.model.Storage;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.anyList;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Nurika Dewi <ikaa.nurr@gmail.com>.
 * @since 8/1/17.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Observable.class, AndroidSchedulers.class, Looper.class, Contact.class})
public class ContactPresenterTest {

    public static final String TEST_ERROR_MESSAGE = "error_message";

    @InjectMocks
    private ContactPresenter presenter;
    @Mock
    private IApiService mApiService;
    @Mock
    private ContactMapper mContactMapper;
    @Mock
    private Storage mStorage;
    @Mock
    private IContactView mView;
    @Mock
    private Observable<Contact[]> mObservable;

    @Before
    public void setUp() throws Exception {
        initMocks(this);

        ArrayList<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact());
        when(mStorage.getSavedContacts()).thenReturn(contacts);
    }

    @Test
    public void getContacts() throws Exception {
        PowerMockito.mockStatic(Looper.class);
        when(mApiService.getContacts()).thenReturn(mObservable);
        presenter.getContacts();
        verify(mView, atLeastOnce()).onShowDialog("Please Wait..");
    }

    @Test
    public void onCompleted() throws Exception {
        presenter.onCompleted();
        verify(mView, times(1)).onHideDialog();
    }

    @Test
    public void onError() throws Exception {
        presenter.onError(new Throwable(TEST_ERROR_MESSAGE));
        verify(mView, times(1)).onHideDialog();
        verify(mView, times(1)).onShowToast("Error loading data : " + TEST_ERROR_MESSAGE);
    }

    @Test
    public void onNext() throws Exception {
        ContactResponse response = mock(ContactResponse.class);
        Contact[] responseCakes = new Contact[1];
        when(response.getContacts()).thenReturn(responseCakes);
        presenter.onNext(response.getContacts());

        verify(mContactMapper, times(1)).mapContacts(mStorage, response.getContacts());
        verify(mView, times(1)).onClearItems();
        verify(mView, times(1)).onContactsLoaded(anyList());
    }

    @Test
    public void getContactsFromDatabase() throws Exception {
        presenter.getContactsFromDatabase();
        verify(mView, times(1)).onClearItems();
        verify(mView, times(1)).onContactsLoaded(anyList());
    }
}